-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2018 at 08:01 AM
-- Server version: 5.6.20
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `default_ci3`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `ccode` varchar(2) NOT NULL DEFAULT '',
  `country` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`ccode`, `country`) VALUES
('AF', 'Afghanistan'),
('AX', 'Åland Islands'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American Samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and Herzegovina'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('BN', 'Brunei Darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo'),
('CD', 'Congo, The Democratic Republic of the'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('CI', 'Côte D\'Ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GF', 'French Guiana'),
('PF', 'French Polynesia'),
('TF', 'French Southern Territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GG', 'Guernsey'),
('GN', 'Guinea'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard Island and McDonald Islands'),
('VA', 'Holy See (Vatican City State)'),
('HN', 'Honduras'),
('HK', 'Hong Kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran, Islamic Republic of'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IM', 'Isle of Man'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JE', 'Jersey'),
('JO', 'Jordan'),
('KZ', 'Kazakhstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, Democratic People\'s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Lao People\'s Democratic Republic'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libyan Arab Jamahiriya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macao'),
('MK', 'Macedonia, The Former Yugoslav Republic of'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia, Federated States of'),
('MD', 'Moldova, Republic of'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('ME', 'Montenegro'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('MP', 'Northern Mariana Islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PS', 'Palestinian Territory, Occupied'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('BL', 'Saint Barthélemy'),
('SH', 'Saint Helena'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('MF', 'Saint Martin'),
('PM', 'Saint Pierre and Miquelon'),
('VC', 'Saint Vincent and the Grenadines'),
('WS', 'Samoa'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('RS', 'Serbia'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('GS', 'South Georgia and the South Sandwich Islands'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and Jan Mayen'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian Arab Republic'),
('TW', 'Taiwan, Province Of China'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, United Republic of'),
('TH', 'Thailand'),
('TL', 'Timor-Leste'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UM', 'United States Minor Outlying Islands'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VE', 'Venezuela'),
('VN', 'Viet Nam'),
('VG', 'Virgin Islands, British'),
('VI', 'Virgin Islands, U.S.'),
('WF', 'Wallis And Futuna'),
('EH', 'Western Sahara'),
('YE', 'Yemen'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `default_classes`
--

CREATE TABLE `default_classes` (
  `id_class` int(11) NOT NULL,
  `class_name` varchar(32) NOT NULL,
  `class_title` varchar(32) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `added_date` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_classes`
--

INSERT INTO `default_classes` (`id_class`, `class_name`, `class_title`, `enabled`, `added_date`, `added_by`, `updated_by`, `updated_date`) VALUES
(1, 'user', 'Users', 1, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00'),
(2, 'user_type', 'User Types', 1, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00'),
(3, 'menu', 'Menu', 1, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00'),
(4, 'myclass', 'Classes', 1, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `default_class_functions`
--

CREATE TABLE `default_class_functions` (
  `id_class_function` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_function_title` varchar(128) NOT NULL,
  `class_function_name` varchar(128) NOT NULL,
  `class_function_type` smallint(1) NOT NULL,
  `class_function_order` int(11) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_class_functions`
--

INSERT INTO `default_class_functions` (`id_class_function`, `class_id`, `class_function_title`, `class_function_name`, `class_function_type`, `class_function_order`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 1, 'View User', 'view_user', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 1, 'Edit User', 'edit_user', 2, 2, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(3, 1, 'Delete User', 'delete_user', 2, 3, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(4, 1, 'Add User', 'add_user', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 2, 'Edit User Type', 'edit_user_type', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 2, 'Delete User Type', 'delete_user_type', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(8, 3, 'View Menu', 'view_menu', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(9, 3, 'Edit Menu', 'edit_menu', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 3, 'Delete Menu', 'delete_menu', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 4, 'Edit Function', 'edit_function', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 4, 'Delete Function', 'delete_function', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `default_configs`
--

CREATE TABLE `default_configs` (
  `id_config` int(11) NOT NULL,
  `config_name` varchar(128) NOT NULL,
  `config_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_configs`
--

INSERT INTO `default_configs` (`id_config`, `config_name`, `config_value`) VALUES
(1, 'EMAIL_PROTOCOL', 'mail'),
(2, 'SMTP_DOMAIN', ''),
(3, 'SMTP_HOST', 'mail.techcellar.net'),
(4, 'SMTP_USER', 'email@techcellar.ne'),
(5, 'SMTP_PASS', 'XC-3#-rc'),
(6, 'SMTP_PORT', '25'),
(7, 'SMTP_CC', 'melin@techcellar.com'),
(8, 'SMTP_BCC', ''),
(9, 'EMAIL_TYPE', 'html'),
(10, 'DEBUG_MODE', '0'),
(11, 'FIXED_MENU', '1'),
(12, 'FIXED_FOOTER', '0'),
(13, 'SITE_TITLE', 'TC'),
(14, 'COMP_LOGO', ''),
(15, 'COMP_NAME', 'Techcellar'),
(16, 'DEFAULT_COUNTRY', 'Philippines'),
(17, 'DEFAULT_TIME_ZONE', 'Asia/Manila'),
(18, 'COPY_RIGHT', 'www.techcellar.com');

-- --------------------------------------------------------

--
-- Table structure for table `default_departments`
--

CREATE TABLE `default_departments` (
  `id_department` int(11) NOT NULL,
  `department_name` varchar(64) NOT NULL,
  `department_code` varchar(8) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_departments`
--

INSERT INTO `default_departments` (`id_department`, `department_name`, `department_code`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 'Information Technology', 'IT', 1, 0, '0000-00-00 00:00:00', 1, '2017-11-03 15:06:57'),
(2, 'Admin', 'Admin', 1, 1, '0000-00-00 00:00:00', 1, '2017-11-03 15:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `default_links`
--

CREATE TABLE `default_links` (
  `id_link` int(11) NOT NULL,
  `parent_link_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `link_url` tinytext NOT NULL,
  `link_name` tinytext NOT NULL,
  `link_title` tinytext NOT NULL,
  `link_detail` tinytext NOT NULL,
  `link_icon` varchar(50) NOT NULL,
  `link_location` int(11) NOT NULL,
  `link_newtab` int(11) NOT NULL,
  `link_external` int(11) NOT NULL,
  `link_head` int(11) NOT NULL,
  `link_order` int(11) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_links`
--

INSERT INTO `default_links` (`id_link`, `parent_link_id`, `user_type_id`, `link_url`, `link_name`, `link_title`, `link_detail`, `link_icon`, `link_location`, `link_newtab`, `link_external`, `link_head`, `link_order`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 0, 1, 'system/profile', 'Profile', '', '', 'icomoon-icon-user', 1, 0, 0, 0, 1, 1, 1, '2015-07-07 14:01:43', 0, '0000-00-00 00:00:00'),
(2, 0, 1, '#', 'Settings', '', '', 'icomoon-icon-cog-2', 2, 0, 0, 1, 11, 1, 1, '2015-07-07 14:03:00', 1, '2015-11-06 13:37:06'),
(3, 2, 1, 'users/list', 'User', '', '', 'brocco-icon-user', 2, 0, 0, 0, 5, 1, 1, '2015-07-07 14:04:03', 1, '2015-11-06 08:42:24'),
(4, 2, 1, 'user_types/list', 'User Type', '', '', 'icomoon-icon-users', 2, 0, 0, 0, 2, 1, 1, '2015-07-07 14:07:25', 1, '2015-07-07 14:07:54'),
(5, 2, 1, 'system/myclass/list_class_page', 'Class', '', '', 'entypo-icon-globe', 2, 0, 0, 0, 3, 1, 1, '2015-07-07 14:13:34', 0, '0000-00-00 00:00:00'),
(6, 2, 1, 'system/menu/list_typemenu', 'Menu', '', '', 'icomoon-icon-menu-2', 2, 0, 0, 0, 1, 1, 1, '2015-07-07 14:14:22', 1, '2015-11-06 08:42:37'),
(7, 2, 1, 'system/myclass/user_access', 'Access', '', '', 'icomoon-icon-lock', 2, 0, 0, 0, 4, 1, 1, '2015-07-07 14:14:57', 1, '2017-11-07 15:01:24'),
(8, 2, 1, 'departments/list', 'Departments', '', '', 'brocco-icon-user', 2, 0, 0, 0, 6, 1, 1, '2015-07-07 14:04:03', 1, '2015-11-06 08:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `default_notifications`
--

CREATE TABLE `default_notifications` (
  `id_notification` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `link` varchar(64) NOT NULL,
  `status` text NOT NULL,
  `todo` tinyint(1) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `done` tinyint(1) NOT NULL,
  `approved` int(11) NOT NULL,
  `seen_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_notifications`
--

INSERT INTO `default_notifications` (`id_notification`, `class_id`, `reference_id`, `user_id`, `link`, `status`, `todo`, `seen`, `done`, `approved`, `seen_date`) VALUES
(1, 1, 1, 1, 'view_user', 'View Newly Added User', 1, 0, 0, 0, '0000-00-00 00:00:00'),
(2, 1, 1, 1, 'view_user', 'View Newly Added User', 1, 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `default_users`
--

CREATE TABLE `default_users` (
  `id_user` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_fname` varchar(64) NOT NULL,
  `user_mname` varchar(64) NOT NULL,
  `user_lname` varchar(64) NOT NULL,
  `user_code` varchar(64) NOT NULL,
  `user_email` varchar(64) NOT NULL,
  `user_contact` varchar(32) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_street` text NOT NULL,
  `user_city` varchar(64) NOT NULL,
  `user_province` varchar(64) NOT NULL,
  `user_country` varchar(64) NOT NULL,
  `user_picture` text NOT NULL,
  `user_resetkey` varchar(64) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_users`
--

INSERT INTO `default_users` (`id_user`, `user_type_id`, `department_id`, `user_fname`, `user_mname`, `user_lname`, `user_code`, `user_email`, `user_contact`, `user_password`, `user_street`, `user_city`, `user_province`, `user_country`, `user_picture`, `user_resetkey`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 1, 1, 'TechCellar', '', 'Admin', 'admin', 'email@techcellar.com', 'Contact', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Street', 'City', 'Province', 'Country', '0a392013e090efcba7ce1a0965933286.png', '', 1, 0, '0000-00-00 00:00:00', 1, '2017-10-12 15:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `default_user_accesses`
--

CREATE TABLE `default_user_accesses` (
  `id_user_access` int(11) NOT NULL,
  `class_function_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_access_status` int(11) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_user_types`
--

CREATE TABLE `default_user_types` (
  `id_user_type` int(11) NOT NULL,
  `user_type` varchar(64) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_user_types`
--

INSERT INTO `default_user_types` (`id_user_type`, `user_type`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 'Super Admin', 1, 0, '0000-00-00 00:00:00', 2, '2017-07-12 15:04:40'),
(2, 'Manager', 1, 0, '0000-00-00 00:00:00', 1, '2017-02-06 14:03:48'),
(3, 'Employee', 0, 1, '2017-02-03 14:56:54', 1, '2017-03-28 14:06:42'),
(4, 'test', 0, 2, '2017-07-10 14:11:24', 2, '2017-07-12 15:05:19'),
(5, 'test2', 0, 2, '2017-07-10 14:12:14', 2, '2017-07-11 14:59:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`ccode`);

--
-- Indexes for table `default_classes`
--
ALTER TABLE `default_classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `default_class_functions`
--
ALTER TABLE `default_class_functions`
  ADD PRIMARY KEY (`id_class_function`);

--
-- Indexes for table `default_configs`
--
ALTER TABLE `default_configs`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `default_departments`
--
ALTER TABLE `default_departments`
  ADD PRIMARY KEY (`id_department`);

--
-- Indexes for table `default_links`
--
ALTER TABLE `default_links`
  ADD PRIMARY KEY (`id_link`);

--
-- Indexes for table `default_notifications`
--
ALTER TABLE `default_notifications`
  ADD PRIMARY KEY (`id_notification`);

--
-- Indexes for table `default_users`
--
ALTER TABLE `default_users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `default_user_accesses`
--
ALTER TABLE `default_user_accesses`
  ADD PRIMARY KEY (`id_user_access`);

--
-- Indexes for table `default_user_types`
--
ALTER TABLE `default_user_types`
  ADD PRIMARY KEY (`id_user_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `default_classes`
--
ALTER TABLE `default_classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `default_class_functions`
--
ALTER TABLE `default_class_functions`
  MODIFY `id_class_function` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `default_configs`
--
ALTER TABLE `default_configs`
  MODIFY `id_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `default_departments`
--
ALTER TABLE `default_departments`
  MODIFY `id_department` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `default_links`
--
ALTER TABLE `default_links`
  MODIFY `id_link` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `default_notifications`
--
ALTER TABLE `default_notifications`
  MODIFY `id_notification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `default_users`
--
ALTER TABLE `default_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_user_accesses`
--
ALTER TABLE `default_user_accesses`
  MODIFY `id_user_access` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_user_types`
--
ALTER TABLE `default_user_types`
  MODIFY `id_user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
