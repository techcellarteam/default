<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// --------------------------------------------------------------------

/**
* MY_Session Class
*
* Extends CI_Session library
*
*/
class MY_Session extends CI_Session
{
	public $CI;

	// ------------------------------------------------------------------------

	/*
	* Constructor
	*
	* Called automatically
	* Inherits method from the parent class
	*/
	public function __construct(){
//		parent::__construct(); //Message: ini_set(): A session is active. You cannot change the session module's ini settings at this time
	}

	// ------------------------------------------------------------------------

	/*
	* Get session data
	*
	* Extends userdata() function
	*/
	public function get($session_name = '', $reference = ''){
		$CI =& get_instance();

		if ( ! empty($reference))
		if ($CI->session->userdata($session_name)){
			if (array_key_exists($reference, $CI->session->userdata[$session_name]))
			return $CI->session->userdata[$session_name][$reference];
			else
			return FALSE;
		}
		return $CI->session->userdata($session_name);
	}

	// ------------------------------------------------------------------------

	/*
	* Get flash session data
	*
	* Extends flash_userdata() function
	*/
	public function getFlash($session_name = ''){
		$CI =& get_instance();
		return $CI->session->flashdata($session_name);
	}

	// ------------------------------------------------------------------------

	/*
	* Do not update an existing session on ajax calls
	*
	* @access public
	* @return void
	*/
	public function sess_update(){
		if ( ! IS_AJAX){
			parent::sess_update();
		}
	}

	function sess_destroy(){
		parent::sess_destroy();
		$this->userdata = array();
	}
}