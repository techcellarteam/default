<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Types extends System_Core {

    function __construct() {

        parent:: __construct();
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('system/user_accesses_model');
        $this->load->model('departments/departments_model');
        $this->load->model('users/users_model');
        $this->load->model('user_types/user_types_model');

        $this->load->model('system/default_model');
        $this->load->model('system/class_functions_model');
        //set list contents
    }

    function index() {
        
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_user_type() {
        $data = array(
            'template' => parent::main_template(),
            'css_files' => $this->config->item('css_for_tables'),
            'js_files' => $this->config->item('js_for_tables'),
        );
        $this->load->view(usertypes_dir('user_types/list'), $data);
    }

    function add_user_type() {
        $data = array(
            'css_files' => $this->config->item('css_for_validation'),
            'js_files' => $this->config->item('js_for_validation'),
            'template' => parent::main_template(),
        );
        $this->load->view(usertypes_dir('user_types/add'), $data);
    }

    function edit_user_type() {
        $id_user_type = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check User if Exist */
        $row = $this->user_types_model->getFields($id_user_type);
        if ($row) {
            $data = array(
                'css_files' => $this->config->item('css_for_validation'),
                'js_files' => $this->config->item('js_for_validation'),
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(usertypes_dir('user_types/edit'), $data);
        } else {
            redirect(usertypes_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_user_type() {
        $id_user_type = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check User Type if Exist */
        $row = $this->user_types_model->getFields($id_user_type);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(usertypes_dir('user_types/view'), $data);
        } else {
            redirect(usertypes_dir('profile/view_pagenotfound_page'));
        }
    }

    function access_user_page() {
        $data = array(
            'template' => parent::main_template(),
            'user_types' => $this->user_types_model->getSearch(array('id_user_type !=' => 1), "", array("user_type" => "ASC"), true),
            'classes' => $this->classes_model->getSearch("", "", array("class_name" => "ASC"), true)
        );

        $this->load->view(usertypes_dir('user/access_user_page'), $data);
    }

    function get_users_select() {
        $department_id = $this->tools->getPost('department_id');
        /* Check Department if Exist */
        $row = $this->users_model->getList(array('department_id' => $department_id))->result();
        if ($row) {
            $data = array(
                'row' => $row,
            );
            $this->load->view(usertypes_dir('user/extra/get_users_select'), $data);
        } else {
            redirect(usertypes_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_user_type') { /* Method for User Type */
            self::_method_list_user_type();
        } else if ($this->uri->rsegment(3) == 'add_user_type') {
            self::_method_add_user_type();
        } else if ($this->uri->rsegment(3) == 'edit_user_type') {
            self::_method_edit_user_type();
        } else if ($this->uri->rsegment(3) == 'delete_user_type') {
            self::_method_delete_user_type();
        }
    }

    /* -------------------------------USER ACCESS---------------------------------------------------------------------------------------- */

    /*
     * Change User Access 
     */

    function _method_change_access() {
        if (IS_AJAX) {
            $user_access_status = ($this->input->post('check') == 'true') ? 1 : 0;
            $status = ($this->input->post('check') == 'true') ? 'Active' : 'Not Active';
            $id_user_type = $this->input->post('usertype');
            $id_class_function = $this->input->post('item');
            $row = $this->user_accesses_model->getSearch(array("ut.id_user_type" => $id_user_type, "cf.id_class_function" => $id_class_function), "", "", "", "", true);
            if ($row) {
                $datas = array(
                    'user_access_status' => $user_access_status,
                    'user_type_id' => $id_user_type,
                    'class_function_id' => $id_class_function,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                $this->user_accesses_model->update_table($datas, array("id_user_access" => $row->id_user_access));
                parent::save_log($row->id_user_access, "changed " . $row->user_type . " user access " . $row->class_function_title . " $status");
            } else if ($id_user_type != 1) {
                $datas = array(
                    'user_access_status' => $user_access_status,
                    'enabled' => 1,
                    'user_type_id' => $id_user_type,
                    'class_function_id' => $id_class_function,
                    'added_by' => $this->my_session->get('admin', 'user_id'),
                    'added_date' => date('Y-m-d H:i:s')
                );
                $id_user_access = $this->user_accesses_model->insert_table($datas);
                $user_type = $this->user_types_model->getValue($id_user_type, "user_type");
                $class_function_title = $this->class_functions_model->getValue($id_class_function, "class_function_title");
                parent::save_log($id_user_access, "changed $user_type user access $class_function_title active");
            }
        }
    }

    /*
     * 	List Access Class and Function
     */

    function _method_list_classfunction() {
        if (IS_AJAX) {

            $id_user_type = $this->input->post('usertype');
            /* Check User Type if Exist */
            $row = $this->user_types_model->getFields($id_user_type);

            if ($row) {
                $class = (!empty($this->input->post('class'))) ? array('c.id_class' => $this->input->post('class')) : array();
                $data = array(
                    'class_functions' => $this->class_functions_model->classes_data($class),
                    'classes' => $this->classes_model->getSearch($class, "", array("class_title" => "ASC"), true),
                    'user_accesses' => $this->Misc->query_to_arr($this->user_accesses_model->getSearch(array("ut.id_user_type" => $id_user_type, "user_access_status" => 1), '', '', true), 'id_class_function'),
                    'id_user_type' => $id_user_type
                );
                $this->load->view(usertypes_dir('user/method/list_classfunction'), $data);
            }
        }
    }

    /* -------------------------------USER TYPE--------------------------------------------------------------------------------------- */

    /*
     * 	Delete User Type Method
     */

    function _method_delete_user_type() {
        if (IS_AJAX) {
            $id_user_type = $this->Misc->decode_id($this->input->post('item'));
            /* Check User Type if Exist */
            $row = $this->user_types_model->getFields($id_user_type);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete User Type */
                $this->user_types_model->update_table($datas, array("id_user_type" => $id_user_type));
                parent::save_log($id_user_type, "deleted user type " . $row->user_type);
                $arr = array(
                    'message_alert' => "Successfully Deleted $row->user_type User Type",
                    'redirect' => $this->input->post('redirect'),
                    'status' => "success",
                    'id' => $id_user_type
                );
                $this->load->view(usertypes_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Edit User Type Method
     */

    function _method_edit_user_type() {
        if (IS_AJAX) {
            $id_user_type = $this->Misc->decode_id($this->input->post('id'));
            /* Check User Type if Exist */
            $row = $this->user_types_model->getFields($id_user_type);
            if ($row) {
                $this->form_validation->set_rules('code', 'User Type', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $arr = array(
                        'message_alert' => validation_errors(),
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                } else {
                    /* Check User Type Exist */
                    $count = $this->user_types_model->getSearch(array("id_user_type !=" => $id_user_type, "user_type like" => $this->input->post('code')), "", "", "", true);
                    if ($count == 0) {
                        $datas = array(
                            'user_type' => $this->input->post('code'),
                            'updated_by' => $this->my_session->get('admin', 'user_id'),
                            'updated_date' => date('Y-m-d H:i:s')
                        );
                        /* Update User Type Info */
                        $this->user_types_model->update_table($datas, array("id_user_type" => $id_user_type));
                        parent::save_log($id_user_type, "updated user type " . $this->Misc->update_name_log($row->user_type, $this->input->post('code')));
                        $arr = array(
                            'message_alert' => " Successfully Saved User Type " . $this->input->post('code') . ".",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "success",
                            'id' => $id_user_type
                        );
                    } else {
                        $arr = array(
                            'message_alert' => "User Type " . $this->input->post('code') . " Already Exists",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "error",
                            'id' => 0
                        );
                    }
                }
                $this->load->view(usertypes_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Add New User Type Method
     */

    function _method_add_user_type() {
        if (IS_AJAX) {
            $this->form_validation->set_rules('code', 'User Type', 'htmlspecialchars|trim|required');

            if ($this->form_validation->run() == FALSE) {
                $arr = array(
                    'message_alert' => validation_errors(),
                    'redirect' => $this->input->post('redirect'),
                    'status' => "error",
                    'id' => 0
                );
            } else {
                /* Check User Type Code Name Exist */
                $count = $this->user_types_model->getSearch(array("user_type like" => $this->input->post('code')), "", "", "", true);
                if ($count == 0) {
                    $datas = array(
                        'user_type' => $this->input->post('code'),
                        'enabled' => 1,
                        'added_by' => $this->my_session->get('admin', 'user_id'),
                        'added_date' => date('Y-m-d H:i:s')
                    );
                    /* Add New Class */
                    $id_user_type = $this->user_types_model->insert_table($datas);
                    parent::save_log($id_user_type, "added new user type " . $this->input->post('code'));
                    $arr = array(
                        'message_alert' => "Successfully Saved User Type " . $this->input->post('code'),
                        'redirect' => $this->input->post('redirect'),
                        'status' => "success",
                        'id' => $id_user_type
                    );
                } else {
                    $arr = array(
                        'message_alert' => "User Type " . $this->input->post('code') . " already exists",
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                }
            }
            $this->load->view(usertypes_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	List User Type
     */

    function _method_list_user_type() {

        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(system_url($this->classname));
        }

        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'id_user_type',
            ),
            'user_type' => array(
                'label' => 'User Type',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'user_type',
            ),
        );
        $data = array(
            'search' => $this->tools->getPost('search'),
            'sort' => $this->tools->getPost('sort'),
        );


        /* Default Order */
        $sort_data = array(); //default sorting
        if (!empty($this->tools->getPost('sort'))) {
            $sort_by = $this->tools->getPost('sort', 'sort_by');
            $sort_type = $this->tools->getPost('sort', 'sort_type');
            $sort_data = array_merge($sort_data, array($sort_by => $sort_type));
        }

        //set condition for the list
        $condition = array();
        $string_condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_user_type') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%$val%"));
                    }
                }
            }
        }

        $data['list_count'] = $this->user_types_model->getList($condition, $sort_data, $string_condition)->num_rows();
        $data['list'] = $this->user_types_model->getListLimit($condition, $sort_data, $string_condition, $this->page, $this->display);


        $datas = array('page' => $this->page,
            'display' => $this->display,
            'num_button' => $this->num_button,
            'list_count' => $data['list_count'],
            'list' => $data['list'],
            'max_page' => ((ceil($data['list_count'] / $this->display)) <= 0) ? 1 : (ceil($data['list_count'] / $this->display)),
        );
        //column customizaton


        $details = array_merge($data, $this->Misc->get_pagination_data($datas)); //get pagination data
        $this->load->view(usertypes_dir('list/user_type_list'), $details);
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Additional Module --------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */


    /*
     * Show User Type Popup Form
     */

    function popupform_usertype() {
        if (IS_AJAX) {
            $redirect = (!empty($this->input->post('redirect'))) ? $this->input->post('redirect') : "";

            if ($this->input->post('type') == 1) { /* Add Form */
                $this->load->view(usertypes_dir('user/extra/popupform_usertype'), array('add' => 1, 'redirect' => $redirect));
            } else if ($this->input->post('type') == 2 and ! empty($this->input->post('item'))) { /* Edit Form */
                $id_user_type = $this->Misc->decode_id($this->input->post('item'));
                $row = $this->user_types_model->getFields($id_user_type);
                if ($row) {
                    $this->load->view(usertypes_dir('user/extra/popupform_usertype'), array('edit' => 1, 'row' => $row, 'redirect' => $redirect));
                }
            }
        }
    }

}
