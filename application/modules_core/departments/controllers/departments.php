<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Departments extends System_Core {

    function __construct() {
        parent:: __construct();
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('departments/departments_model');
    }

    /* -------------------------------------------------------------------- */
    /* ------------------------------- Pages ------------------------------ */
    /* -------------------------------------------------------------------- */

    function add_department() {
        $data = array(
            'template' => parent::main_template(),
            'css_files' => $this->config->item('css_for_validation'),
            'js_files' => $this->config->item('js_for_validation'),
        );
        $this->load->view(departments_dir('departments/add'), $data);
    }

    function edit_department() {
        $id_dept = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check if Department Exists */
        $row = $this->departments_model->getFields($id_dept);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'css_files' => $this->config->item('css_for_validation'),
                'js_files' => $this->config->item('js_for_validation'),
                'row' => $row,
            );
            $this->load->view(departments_dir('departments/edit'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_department() {
        $id_dept = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check if Department Exists */
        $row = $this->departments_model->getFields($id_dept);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(departments_dir('departments/view'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function list_departments() {
        $data = array(
            'template' => parent::main_template(),
            'css_files' => $this->config->item('css_for_tables'),
            'js_files' => $this->config->item('js_for_tables'),
        );
        $this->load->view(departments_dir('departments/list'), $data);
    }

    /* --------------------------------------------------------------------- */
    /* ------------------------------- Method ------------------------------ */
    /* --------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_departments') { /* Method for Department */
            self::_method_list_departments();
        } else if ($this->uri->rsegment(3) == 'add_department') {
            self::_method_add_department();
        } else if ($this->uri->rsegment(3) == 'edit_department') {
            self::_method_edit_department();
        } else if ($this->uri->rsegment(3) == 'delete_department') {
            self::_method_delete_department();
        }
    }

    function _method_list_departments() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(system_url($this->classname));
        }

        $data = array(
            'search' => $this->input->post('search'),
            'sort' => $this->input->post('sort'),
        );

        /* Default Order */
        $sort_data = array(); //default sorting
        if (!empty($this->input->post('sort'))) {
            $sort = $this->input->post('sort');
            $sort_by = $sort['sort_by'];
            $sort_type = $sort['sort_type'];
            switch ($sort_by) {
                case 'user_name':
                    $sort_by = 'user_mname';
                    break;
                default:
                    # code...
                    break;
            }
            $sort_data = array_merge($sort_data, array($sort_by => $sort_type));
        }

        //set condition for the list
        $condition = array();
        $string_condition = array();


        if (!empty($this->input->post('search'))) {
            foreach ($this->input->post('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_user') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%$val%"));
                    }
                }
            }
        }

        $data['list_count'] = $this->departments_model->getList($condition, $string_condition, $sort_data)->num_rows();

        $data['list'] = $this->departments_model->getListLimit($condition, $string_condition, $sort_data, $this->page, $this->display);

        $datas = array('page' => $this->page,
            'display' => $this->display,
            'num_button' => $this->num_button,
            'list_count' => $data['list_count'],
            'list' => $data['list'],
            'max_page' => ((ceil($data['list_count'] / $this->display)) <= 0) ? 1 : (ceil($data['list_count'] / $this->display)),
        );
        $details = array_merge($data, $this->Misc->get_pagination_data($datas)); //get pagination data
        $this->load->view(departments_dir('list/department_list'), $details);
    }

    function _method_add_department() {
        if (IS_AJAX) {

            $this->form_validation->set_rules('deptname', 'Department Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('deptcode', 'Department Code', 'htmlspecialchars|trim|required');

            if ($this->form_validation->run() == FALSE) {
                $arr = array(
                    'message_alert' => validation_errors(),
                    'redirect' => $this->input->post('redirect'),
                    'status' => "error",
                    'id' => 0
                );
            } else {
                /* Check Department Exists */
                $department = $this->departments_model->getSearch(array("(department_name LIKE '%{$this->input->post('deptname')}%' OR department_code LIKE '%{$this->input->post('deptcode')}%')" => NULL), "", "", "", "", "");

                if ($department->num_rows() == 0) {
                    $data = array(
                        'department_name' => $this->input->post('deptname'),
                        'department_code' => $this->input->post('deptcode'),
                        'enabled' => 1,
                        'added_by' => $this->my_session->get('admin', 'user_id'),
                        'added_by' => date('Y-m-d H:i:s')
                    );

                    /* Add Department Info */
                    $id_department = $this->departments_model->insert_table($data);

                    parent::save_log($id_department, "added department {$data['department_name']}");
                    $arr = array(
                        'message_alert' => "Successfully Added Department {$data['department_name']}.",
                        'redirect' => $this->input->post('redirect'),
                        'status' => "success",
                        'id' => $id_department
                    );
                } else {
                    $arr = array(
                        'message_alert' => "Department already exists.",
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                }
            }
            $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    function _method_edit_department() {
        if (IS_AJAX) {
            $id_department = $this->Misc->decode_id($this->input->post('id'));
            /* Check if Department Exists */
            $row = $this->departments_model->getFields($id_department);
            if ($row) {
                $this->form_validation->set_rules('deptname', 'Department Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('deptcode', 'Department Code', 'htmlspecialchars|trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $arr = array(
                        'message_alert' => validation_errors(),
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                } else {
                    /* Check if Edited Department Exists */
                    $department = $this->departments_model->getSearch(array("(department_name LIKE '%{$this->input->post('deptname')}%' OR department_code LIKE '%{$this->input->post('deptcode')}%')" => NULL), "", "", "", "", true);

                    if ($department->id_department == $id_department) {
                        $count = 0;
                    } else {
                        $count = 1;
                    }

                    if ($count == 0) {
                        $datas = array(
                            'department_name' => $this->input->post('deptname'),
                            'department_code' => $this->input->post('deptcode'),
                            'updated_by' => $this->my_session->get('admin', 'department_id'),
                            'updated_date' => date('Y-m-d H:i:s')
                        );

                        /* Update Department Info */
                        $this->departments_model->update_table($datas, array("id_department" => $id_department));

                        parent::save_log($id_department, "updated department $row->department_name");
                        $arr = array(
                            'message_alert' => "Successfully Updated Department.",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "success",
                            'id' => $id_department
                        );
                    } else {
                        $arr = array(
                            'message_alert' => "Department already exists.",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "error",
                            'id' => 0
                        );
                    }
                }
                $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    function _method_delete_department() {
        if (IS_AJAX) {
            $id_department = $this->Misc->decode_id($this->input->post('item'));
            /* Check User if Exist */
            $row = $this->departments_model->getFields($id_department);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete User */
                $this->departments_model->update_table($datas, array("id_department" => $id_department));
                parent::save_log($id_department, "deleted department {$row->department_name}");
                $arr = array(
                    'message_alert' => "Department $row->department_code Deleted ",
                    'redirect' => $this->input->post('redirect'),
                    'status' => "success",
                    'id' => $id_department
                );
                $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}

?>
