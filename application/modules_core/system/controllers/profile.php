<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends System_Core {

    public $users_model = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->function_name = 'profile'; // use to call functions for access

        parent:: __construct();

        $this->load->model('users/users_model');
        $this->users_model = new users_model();

        $this->contents = array('model_directory' => 'system/users_model',
            'model_name' => 'users_model',
            'filters' => array(),
            'functionName' => 'Profile Page',); // use to call functions for access
    }

    function index() {
        redirect('system/profile/view_profile_page');
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function view_pagenotfound_page() {
        $this->load->view(system_dir('profile/view_pagenotfound_page'));
    }

    function view_profile_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $result
            );
            $this->load->view(system_dir('profile/view_profile_page'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_password_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'css_files' => $this->config->item('css_for_validation'),
                'js_files' => $this->config->item('js_for_validation'),
                'result' => $result
            );
            $this->load->view(system_dir('profile/edit_password_page'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_myinfo_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'css_files' => $this->config->item('css_for_validation'),
                'js_files' => $this->config->item('js_for_validation'),
                'result' => $result
            );
            $this->load->view(system_dir('profile/edit_myinfo_page'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function take_picture_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'result' => $result
            );
            $this->load->view(system_dir('profile/take_picture_page'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'edit_password') {
            self::_method_edit_password();
        } else if ($this->uri->rsegment(3) == 'upload_mypicture') {
            self::_method_upload_mypicture();
        } else if ($this->uri->rsegment(3) == 'edit_myinfo') {
            self::_method_edit_myinfo();
        }
    }

    function _method_edit_myinfo() {
        if (IS_AJAX) {
            $id_user = $this->my_session->get('admin', 'user_id');
            $this->form_validation->set_rules('fname', 'First Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('mname', 'Middle Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('street', 'Street', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('city', 'City', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('province', 'Province', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('country', 'Country', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('contact', 'Contact', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $arr = array(
                    'message_alert' => validation_errors(),
                    'redirect' => $this->input->post('redirect'),
                    'status' => "error",
                    'id' => 0
                );
            } else {
                $datas = array(
                    'user_fname' => $this->input->post('fname'),
                    'user_lname' => $this->input->post('lname'),
                    'user_mname' => $this->input->post('mname'),
                    'user_street' => $this->input->post('street'),
                    'user_city' => $this->input->post('city'),
                    'user_province' => $this->input->post('province'),
                    'user_country' => $this->input->post('country'),
                    // 'user_email'=>$this->input->post('email'),
                    'user_contact' => $this->input->post('contact')
                );
                /* Update User Info */
                $this->users_model->update_table($datas, array("id_user" => $id_user));
                parent::save_log($id_user, "updated information");
                $arr = array(
                    'message_alert' => "Successfully saved.",
                    'redirect' => $this->input->post('redirect'),
                    'status' => "success",
                    'id' => $id_user
                );
            }
            $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    function _method_upload_mypicture() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $path = parent::upload_user_path($id_user);
        if (!empty($this->input->post('image_data'))) {
            /* Convert Base64 to Picture */
            $convert = parent::convert_image($this->input->post('image_data'), $path['profile'], $path['profile_thumb']);
            if ($convert[0]) {
                $datas = array(
                    'user_picture' => $convert['file_name'],
                    'updated_by' => $id_user,
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Update User Picture */
                $this->users_model->update_table($datas, array("id_user" => $id_user));
                parent::save_log($id_user, "took a picture");
                $arr = array(
                    'message_alert' => "Successfully Saved.",
                    'status' => "success",
                    'id' => 1
                );
            } else {
                $arr = array(
                    'message_alert' => $convert['error'],
                    'status' => "error",
                    'id' => 0
                );
            }
        } else if (!empty($_FILES)) {
            $filetype = 'gif|jpg|png';
            /* Upload Picture */
            $upload = parent::upload_file($path['profile'], $path['profile_thumb'], 'file', $filetype, '2048');
            if ($upload[0]) {
                $datas = array(
                    'user_picture' => $upload['orig_name'],
                    'updated_by' => $id_user,
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Update User Picture */
                $this->users_model->update_table($datas, array("id_user" => $id_user));
                parent::save_log($id_user, "uploaded a picture");
                $arr = array(
                    'message_alert' => "Successfully Saved.",
                    'status' => "success",
                    'id' => 1
                );
            } else {
                $arr = array(
                    'message_alert' => $upload['error'],
                    'status' => "error",
                    'id' => 0
                );
            }
        } else {
            $arr = array(
                'message_alert' => "No file.",
                'status' => "error",
                'id' => 0
            );
        }
        $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
    }

    function _method_edit_password() {
        if (IS_AJAX) {
            $id_user = $this->my_session->get('admin', 'user_id');
            $this->form_validation->set_rules('oldpassword', 'Old Password', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('newpassword', 'New Password', 'htmlspecialchars|trim|required|matches[confirmpassword]');
            $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $arr = array(
                    'message_alert' => validation_errors(),
                    'redirect' => $this->input->post('redirect'),
                    'status' => "error",
                    'id' => 0
                );
            } else {
                /* Check Old Password */
                $old_password = sha1($this->input->post('oldpassword'));
                $result = $this->users_model->getSearch(array("u.id_user" => $id_user, "u.user_password like" => $old_password), "", "", "", true);
                if ($result) {
                    $datas = array(
                        'user_password' => sha1($this->input->post('newpassword')),
                        'updated_by' => $id_user,
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    /* Save New Password */
                    $this->users_model->update_table($datas, array("id_user" => $id_user));
                    parent::save_log($id_user, "changed password");

                    $arr = array(
                        'message_alert' => "Password Successfully Changed.",
                        'redirect' => $this->input->post('redirect'),
                        'status' => "success",
                        'id' => $id_user
                    );
                } else {
                    $arr = array(
                        'message_alert' => "Wrong Old Password.",
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                }
            }
            $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Additional Module --------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
}
